#include "../include/ptresidual.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <iostream>
#include <vector>
#include <stdlib.h>
#include <typeinfo>
#include "TVector3.h"
#include "TVector2.h"

using namespace std;

Int_t ptresidual::RPC_Run2(float offline_pt)
{
    float pT = 0;
    float dr = 100;
    for(int j=0;j!=muctpi_ndatawords;j++){
        if(muctpi_source->at(j)!=0)continue;
        if(muctpi_candidateVetoed->at(j)==1)continue;
        
        int rpc_pt=muctpi_thrNumber->at(j);
        float Run3_pt=9999999;
        if(rpc_pt==1){Run3_pt=4;}
        if(rpc_pt==2){Run3_pt=6;}
        if(rpc_pt==3){Run3_pt=7;}
        if(rpc_pt==4){Run3_pt=11;}
        if(rpc_pt==5){Run3_pt=14;}
        if(rpc_pt==6){Run3_pt=14;}
        if(Run3_pt>20){continue;}

        float rpc_eta = (*muctpi_eta)[j];
        float rpc_phi = (*muctpi_phi)[j];
        float deta = abs(rpc_eta - extEta);
        float dphi = TVector2::Phi_mpi_pi(rpc_phi - extPhi);
        float dR = sqrt(deta*deta + dphi*dphi);
        if(dR<dr){
            dr=dR;
            pT=Run3_pt;
        }
    }
    if(dr<=0.1){return pT;}
    else{return 0;}
}
